#! /bin/bash

# make sure the output file exists and is empty
echo "" > $3

 java -cp target/nwgc_hw-1.0-SNAPSHOT.jar org.example.fastqWriter.BinaryToFastqConverter $1 $2 $3
