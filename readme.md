I've written solutions for problems 1 and 3, hopefully the names make clear which is which.

The Memory class (for the random line problem) reads each line of the input up to the given index, and stores the offset of the start of each line.
Then it serializes itself to a file before returning the line. 
On each run, we try to find a serialized Memory object corresponding to the given input file.
If one is found, it will either use its list of offsets to find the desired index, or expand its index up to the desired line.
For an input with 1 billion lines, the offsets for the entire file would be 8GB, so I think the size of the Memory object (both in memory and serialized) should stay fairly manageable.

For the FASTQ problem: 
A BinaryToFastqConverter object takes an input file and a read length, then reads the input file in chunks of that size.
It uses each chunk to create a DnaChunk object, which translates bytes into strings in FASTQ format.
The BinaryToFastqConverter can then write the FASTQ representation of each of its DnaChunks to a file.

I used jdk v11, but I don't think I'm using anything new to that version, I think it would work at least back to 8.

Building and running the solutions:
1. Run: `mvn clean package` to build the jar
2. I wrote some tiny little convenience scripts to wrap the java commands for each program
3. To run the FASTQ converter: `./fastq_writer.sh input.bin 2 out.fastq`
4. To run the random line program: `./random_line.sh kmsp_norms.csv 150`

I wrote everything assuming that input and output files would be in the working directory, 
and that the working directory would be the root of the project, just to avoid messing with file paths too much.
I've tried to mark all the places where I left exceptions or edge cases unhandled, so if it's a mistake that's not marked, it's likely a real mistake.
