package org.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.example.fastqWriter.BinaryToFastqConverter;
import org.example.fastqWriter.DnaChunk;
import org.example.randomLine.Memory;

import static org.example.fastqWriter.DnaChunk.BASE_TABLE;

/**
 * Basically a scratch file, just tests and experiments.
 */
public class Main {
  public static void main(String[] args) throws IOException {
    File testFile = new File("kmsp_norms.csv");
    String s = Memory.getLine(testFile, 10);
    System.out.println(s);
    s = Memory.getLine(testFile, 0);
    System.out.println(s);
    s = Memory.getLine(testFile, 100);
    System.out.println(s);
    s = Memory.getLine(testFile, 366);
    System.out.println(s);

    short[] input = new short[] {0b00000000, 0b11100000, 0b11000001, 0b01111111};
    short[] input1 = new short[] {0b00000000, 0b11100000};
    short[] input2 = new short[] {0b11000001, 0b01111111};

    System.out.println(Arrays.toString(input));

    for (short i : input) {
      int b = i >>> 6;
      char base = BASE_TABLE[b >>> 6];
      System.out.println(b);
      System.out.println(base);

      byte score = (byte) (i & 63);
      System.out.println(score);
      System.out.println((char) (score + 33));
      System.out.println();
    }

    DnaChunk chunk1 = new DnaChunk(input1, 1);
    DnaChunk chunk2 = new DnaChunk(input2, 2);

    System.out.println(chunk1.toFastq() + chunk2.toFastq());

    File f = new File("input.bin");
    Reader bytestream = new BufferedReader(new InputStreamReader(
        new FileInputStream(f), StandardCharsets.ISO_8859_1));
    int ubyte;
    while ((ubyte = bytestream.read()) != -1) {
      System.out.println(ubyte);
    }

    BinaryToFastqConverter converter = new BinaryToFastqConverter(f, 2);
    converter.writeFastq(new File("testOutput.fastq"));
  }
}