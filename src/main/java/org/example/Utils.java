package org.example;

import java.io.File;

public class Utils {
  public static boolean isReadableFile(File file) {
    return file.isFile() && file.canRead();
  }

  public static void checkFileReadable(File file) {
    if (!isReadableFile(file)) {
      throw new IllegalArgumentException(file.getName() + " is not a readable file");
    }
  }
}
