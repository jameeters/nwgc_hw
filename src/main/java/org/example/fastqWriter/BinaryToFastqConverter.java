package org.example.fastqWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.example.Utils.checkFileReadable;

/**
 * I could make this more memory efficient by reading and writing simultaneously, avoiding storing a
 * list of DnaChunks. But read() method is already enough of a mess without having to include that.
 * <p>
 * Since there's no such thing as an unsigned byte in java, I'm using shorts all over the place.
 * They're also signed, but since a byte will never fill them it causes no problems.
 */
public class BinaryToFastqConverter {

  private final File inputFile;
  private final List<DnaChunk> reads;
  private final int length;

  public BinaryToFastqConverter(File inputFile, int length) {
    checkFileReadable(inputFile);
    this.inputFile = inputFile;
    this.reads = new ArrayList<>();
    this.length = length;
    read();
  }

  private void read() {
    short[] rawChunk = new short[length];

    // figuring out how to read single unsigned bytes had me bamboozled,
    // I was working on something using characters and lots of bit shifting,
    // but it was ugly. This is stolen from: https://stackoverflow.com/a/20346077
    try (Reader reader = new BufferedReader(new InputStreamReader(
        new FileInputStream(inputFile), StandardCharsets.ISO_8859_1))) {

      int ubyte;
      // this assumes that the number of bytes in the file will be a multiple of L, which it
      // should be. Otherwise the reader will give -1 when it goes off the end of the file, so
      // the output will end up with entries (T, @) filling the remainder of the last read
      while ((ubyte = reader.read()) != -1) {
        // It's split up this way to avoid running off the end of the file
        rawChunk[0] = (short) ubyte;
        for (int i = 1; i < length; i++) {
          ubyte = reader.read();
          rawChunk[i] = (short) ubyte;
        }
        reads.add(new DnaChunk(rawChunk, reads.size() + 1));
      }
    } catch (IOException e) {
      // todo
      e.printStackTrace();
    }
  }

  public void writeFastq(File outfile) {
    if (!outfile.canWrite()) {
      throw new IllegalArgumentException("Can't write to output file");
    }
    try (PrintWriter writer = new PrintWriter(outfile)) {
      for (DnaChunk chunk : reads) {
        writer.print(chunk.toFastq());
      }
    } catch (IOException e) {
      // todo
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    if (args.length != 3) {
      throw new IllegalArgumentException("I want 3 arguments: input file, L, and output file");
    }
    File inputFile = new File(args[0]);
    int L = Integer.parseInt(args[1]);
    File outputFile = new File(args[2]);

    BinaryToFastqConverter converter = new BinaryToFastqConverter(inputFile, L);
    converter.writeFastq(outputFile);
  }
}
