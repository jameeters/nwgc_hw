package org.example.fastqWriter;

public class DnaChunk {

  public static final char[] BASE_TABLE = new char[] {'A', 'C', 'G', 'T'};

  final int length;
  final int index;
  private StringBuilder bases;
  private StringBuilder scores;


  public DnaChunk(short[] raw, int index) {
    length = raw.length;
    this.index = index;

    bases = new StringBuilder();
    scores = new StringBuilder();

    for (int i = 0; i < length; i++) {
      // this is a kind of opaque bitwise stuff I never actually use,
      // but it seems appropriate here
      bases.append(BASE_TABLE[raw[i] >>> 6]);
      scores.append((char) ((raw[i] & 63) + 33));
    }
  }

  public String toFastq() {
    return "@READ_" + index + "\n"
           + bases + "\n"
           + "+READ_" + index + "\n"
           + scores + "\n";
  }

}
