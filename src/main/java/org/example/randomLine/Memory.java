package org.example.randomLine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.example.Utils.checkFileReadable;
import static org.example.Utils.isReadableFile;

/**
 * Read through a file to fetch an arbitrary line, remembering the offsets of the end of each line
 * read to make future accesses faster. Memory objects are serialized to a file in the working dir.
 * I've also assumed that the input file is in the working dir.
 * <p>
 * I've skipped really handling any errors or edge cases here.
 */
public class Memory implements Serializable {

  private final File file;
  private int lastIndex;
  private final List<Long> lineStarts;
  private boolean dirty;

  private Memory(File file) {
    checkFileReadable(file);
    this.file = file;
    lineStarts = new ArrayList<>();
    lineStarts.add(0L);
    // always equal to lineStarts.size() - 1, I'm keeping it for clarity
    lastIndex = 0;
    dirty = false;
  }

  /**
   * Return the line at the requested index. If the given index is beyond the end of the file, null
   * will be returned. When looking beyond the end of the file, this method will add a bunch of
   * useless entries to lineStarts, which I don't think is worth fixing here because it won't incur
   * much of a penalty until you go way beyond the end of the file.
   * @param index 0-based line number desired.
   * @return the line at the given index, or null
   */
  private String getLine(int index) {
    if (index < 0) {
      return null;
    }
    String line = "unset";
    try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
      if (index > lastIndex) {
        dirty = true;
        raf.seek(lineStarts.get(lastIndex));
        // you have to read all the way to the end of the line you want anyway
        // may as well write down where it was, so you end up recording
        // the start for line (index + 1).
        while (lastIndex <= index) {
          line = raf.readLine();
          lineStarts.add(raf.getFilePointer());
          lastIndex++;
        }
      } else {
        raf.seek(lineStarts.get(index));
        line = raf.readLine();
      }
    } catch (IOException e) {
      // todo
      throw new RuntimeException(e);
    }
    return line;
  }

  private void serialize() {
    if (dirty) {
      dirty = false;
      File ser = getSerializedMemoryFileFor(file);
      try {
        FileOutputStream fos = new FileOutputStream(ser);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.close();
        fos.close();
      } catch (IOException e) {
        // todo
        e.printStackTrace();
      }
    }
  }

  /**
   * If a Memory for the given file already exists, load it and ask it for the line. Otherwise
   * create a new Memory. If the memory reads new data in the process, it will serialize itself to
   * preserve that data.
   * @param file Input text file
   * @param index 0-based index of line to read
   * @return the line at the given index
   */
  public static String getLine(File file, int index) {
    Memory memory;
    if (memoryExistsFor(file)) {
      memory = loadMemoryFor(file);
    } else {
      memory = new Memory(file);
    }
    String line = memory.getLine(index);
    memory.serialize();
    return line;
  }

  private static Memory loadMemoryFor(File file) {
    File ser = getSerializedMemoryFileFor(file);
    checkFileReadable(ser);
    try {
      FileInputStream fis = new FileInputStream(ser);
      ObjectInputStream ois = new ObjectInputStream(fis);
      return (Memory) ois.readObject();
    } catch (Exception e) {
      // todo
      throw new RuntimeException(e);
    }
  }

  private static File getSerializedMemoryFileFor(File file) {
    // You shouldn't have two different files with the same name
    return new File(file.getName() + ".memory.ser");
  }

  private static boolean memoryExistsFor(File file) {
    File ser = getSerializedMemoryFileFor(file);
    return isReadableFile(ser);
  }

  public static void main(String[] args) {
    if (args.length != 2) {
      throw new IllegalArgumentException("I want exactly two (2) arguments: input_file and index");
    }
    File inputFile = new File(args[0]);
    int index = Integer.parseInt(args[1]);

    System.out.println(getLine(inputFile, index));
  }

}
